
const chess = () => {
    for (let i = 0; i < 8; i++) {
        if (i % 2 === 0) {
            const line = [];
            for(let j = 0; j < 8; j++){
                if(j % 2 === 0){
                    line.push('██');
                }else {
                    line.push('  ');
                }
            }
            console.log(line.join(""))
        } else {
            const line = [];
            for (let j = 1; j < 9; j++) {
                if (j % 2 === 0) {
                    line.push('██');
                } else {
                    line.push('  ');
                }
            }
            console.log(line.join(""))
        }
    }
}
chess();